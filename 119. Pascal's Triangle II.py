class Solution:
    def getRow(self, rowIndex: int) -> List[int]:

            
        if not rowIndex:
            raise ValueError('rowIndex must not be None')
            
        if rowIndex < 0:
            raise ValueError('rowIndex must be 0 or greater integer')
            
        if type(rowIndex) != int:
            raise ValueError('rowIndex must be an integer')
        output = [1] # [1, 3, 3, 1]
        
        for i in range(1, rowIndex+1): # i = 3
            for j in range(i-1, 0, -1): # j = 1
                output[j] = output[j-1] + output[j]
            output.append(1)
            
        return output
    
    # None -> raise ValueError
    # -1 -> raise ValueError
    # 0 -> [1]
    # 1 -> [1,1]
    # 4 -> [1,4,6,4,1]
    # 33 -> 
    # 34 -> raise ValueError
    # '8' -> raise ValueError
    # 6.0 -> raise ValueError
    # [34, 12] -> raise ValueError