class Solution:
    def generate(self, numRows: int) -> List[List[int]]: # O(n**2), O(n**2)
        
        output = [[1]]
        
        for i in range(1, numRows):
            
            new_row = [1]
            
            for j in range(1, i):
                
                new_row.append( output[i-1][j-1] + output[i-1][j] )
                
            new_row.append(1)
            
            output.append(new_row)
        
        return output